package com.classpath.streams;

import com.classpath.streams.model.Product;
import com.classpath.streams.util.ProductUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Client {

    public static void main(String[] args) {


        Predicate<Product> isAppleProduct = product -> product.getCompany().equalsIgnoreCase("Apple");
        //Predicate<Product> isNotAppleProduct = product -> ! product.getCompany().equalsIgnoreCase("Apple");
        Predicate<Product> notAppleProduct = isAppleProduct.negate();
        Predicate<Product> productLessThan50K = product -> product.getPrice() < 50_000;
        Predicate<Product> productMoreThan50K = productLessThan50K.negate();
        Predicate<Product> appleProductLessThan50k = isAppleProduct.and(productLessThan50K);



        //find all the products by Apple brand
       /* List<Product> appleProducts = new ArrayList<>();
        for (Product product: products  ) {
            if (product.getCompany().equalsIgnoreCase("APPLE") && product.getPrice() < 50_000) {
                appleProducts.add(product);
            }
        }

        for (Product product: appleProducts) {
            System.out.println(product);
        }*/

        //declarative style of programming
       /* Stream<Product> appleProducts = products.stream()
                                                .filter(appleProductLessThan50k);

        appleProducts.forEach(product -> System.out.println(product));*/
        List<Product> products = ProductUtils.fetchProducts();
        products.stream()
                .filter(appleProductLessThan50k)
                .forEach(System.out::println);

    }


}
