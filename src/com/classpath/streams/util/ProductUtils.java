package com.classpath.streams.util;

import com.classpath.streams.model.Product;

import java.util.Arrays;
import java.util.List;

public class ProductUtils {

    public static List<Product> fetchProducts() {
        List<Product> products = Arrays.asList(
                new Product(12, 56000, "IPhone-12", "APPLE"),
                new Product(13, 59000, "Samsung-Galaxy-Note", "SAMSUNG"),
                new Product(14, 75000, "IPhone-13", "APPLE"),
                new Product(15, 175000, "Mac-Boo-Pro", "APPLE"),
                new Product(16, 125000, "ThinkPad", "LENOVO"),
                new Product(17, 45000, "IPhone-8", "APPLE")
        );
        return  products;
    }
}
