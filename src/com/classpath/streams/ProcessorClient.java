package com.classpath.streams;

import com.classpath.streams.model.Product;
import com.classpath.streams.util.ProductUtils;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public class ProcessorClient {
    public static void main(String[] args) {
        List<Product> products = ProductUtils.fetchProducts();
/*
        Function<Product, String> mapProductToName = new Function<Product, String>() {
            @Override
            public String apply(Product product) {
                return product.getName();
            }
        } ;
*/
        //method reference
        Function<Product, String> mapProductToName = Product::getName;
        //transformation
        Stream<String> productNamesStream = products.stream()
                .filter(product -> product.getCompany().equalsIgnoreCase("APPLE"))
                .map(mapProductToName);

        productNamesStream.forEach(product -> System.out.println(product));
    }
}
